# Learn more: http://github.com/javan/whenever

job_type :unicorn_start, "cd :path && RAILS_ENV=:environment bundle exec unicorn_rails -c :path/config/unicorn/:environment.rb -D"


every :reboot do
  unicorn_start ''
end
