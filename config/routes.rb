Rails.application.routes.draw do
  mount Graudeejs::API => '/'

  constraints subdomain: 'admin' do
    mount_releaf_at '/' do
      namespace :admin, path: '/' do
        releaf_resources :social_links
        releaf_resources :projects
        releaf_resources :bookmarks

        # get "profile", to: "profile#edit", as: :permissions_user_profile
        # patch "profile", to: "profile#update"
        # post "profile/settings", to: "profile#settings", as: :permissions_user_profile_settings
      end
    end
  end

  node_routes_for(TextPage) do
    get 'show'
  end

  node_routes_for(ProjectsController) do
    get 'index'
  end

  node_routes_for(HomeController) do
    get 'show'
  end

  root to: 'home#root'

  match '/*path', to: 'application#page_not_found', via: %i[get post put patch delete]
end
