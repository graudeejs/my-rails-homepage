Releaf.application.configure do
  # Default settings are commented out

  ### setup menu items and therefore available controllers
  config.menu = [
    'admin/nodes',
    'admin/projects',
    'admin/bookmarks',
    'admin/social_links',
    {
      :name => "permissions",
      :items => %w[releaf/permissions/users releaf/permissions/roles]
    },
    'releaf/i18n_database/translations'
  ]

  # config.additional_controllers = ['admin/profile']

  config.components = [
    Releaf::Core,
    Releaf::I18nDatabase,
    Releaf::Permissions,
    Releaf::Content
  ]

  config.content.resources = { 'Node' => { controller: 'Admin::NodesController' } }

  config.available_locales = ['lv', 'en']
  # config.layout_builder_class_name = 'Admin::Page::LayoutBuilder'
  # config.devise_for 'releaf/admin'

  # TODO figure out how to route custom profiles controller
end
