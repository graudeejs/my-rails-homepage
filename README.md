# Graudeejs homepage
## Requirements
- PostgreSQL database
- Amazon s3 bucket


## Configuration
### Amazon s3 bucket policy template
[Policy generator](http://awspolicygen.s3.amazonaws.com/policygen.html) helps a lot

```json
{
	"Statement": [
		{
			"Effect": "Allow",
			"Action": [
				"s3:*"
			],
			"Resource": ["arn:aws:s3:::graudeejs-dev/*"],
			"Principal": {
				"AWS": ["arn:aws:iam::799656286307:user/graudeejs-dev"]
			}
		}
	]
}
```




