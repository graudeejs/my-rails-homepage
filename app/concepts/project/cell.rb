class Project::Cell < Cell::Concept
  include Cell::Hamlit

  cache :show do
    [I18n.locale, model.id, model.updated_at, Releaf::I18nDatabase::Backend.translations_updated_at]
  end

  def show
    render
  end

  private

  def url_host
    URI(model.url).host
  end

  def year_month(date)
    if date.nil?
      I18n.t("presently", scope: 'projects')
    else
      I18n.l(date)
    end
  end

  def linked_description_html
    TextLinker.new(model.description_html).call
  end

end
