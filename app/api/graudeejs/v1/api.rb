module Graudeejs
  module V1
    class API < Grape::API
      version 'v1', using: :path
      logger Rails.logger

      rescue_from(ActiveRecord::RecordNotFound) do
        error!('Record not found')
      end

      helpers do
        def current_user
          @current_user ||= AdminProfile.where.not(token: nil).find_by(token: params.delete(:token))
        end

        def authorize!
          error!('Unauthorized', 401) unless current_user
        end

        def permitted_params
          declared(params, include_missing: false)
        end
      end

      before do
        authorize!
      end

      resource :bookmarks do
        desc 'create bookmark'
        params do
          requires :title, :url, type: String
          optional :description, :tags, type: String
          optional :public, type: Boolean
        end
        post do
          current_user.bookmarks.create!(permitted_params)
        end

        desc 'list bookmarks'
        get do
          current_user.bookmarks
        end

        desc 'update bookmark'
        params do
          requires :title, :url, type: String
          optional :description, :tags, type: String
          optional :public, type: Boolean
        end
        put ':id' do
          current_user.bookmarks.find(params[:id]).update_attributes!(permitted_params)
        end

        desc 'delete bookmark'
        delete(':id') do
          current_user.bookmarks.find(params[:id]).destroy
        end
      end
    end
  end
end
