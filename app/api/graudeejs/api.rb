module Graudeejs
  class API < Grape::API

    format :json
    prefix :api

    mount Graudeejs::V1::API
    add_swagger_documentation
  end
end
