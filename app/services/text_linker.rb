class TextLinker
  include ActionView::Helpers::TagHelper

  def initialize(text)
    @text = " #{text.to_s} "
  end

  def call
    @text.gsub!(pattern) do |fragment|
      process(fragment)
    end

    @text[1...-1]
  end

  private

  def pattern
    @pattern ||= Regexp.new("([^\w\d])(#{ rules.keys.join('|') })([^\w\d])")
  end

  def process(fragment)
    match = pattern.match(fragment)
    pre = match[1]
    text = match[2]
    post = match[3]

    processor = rules[text]

    if processor.is_a? String
      pre + processor + post
    else
      raise 'not implemented'
    end
  end

  def link(text, uri, new_tab: true)
    attributes = { href: uri }
    attributes[:target] = :_blank if new_tab
    content_tag(:a, text, attributes)
  end

  def rules
    {
      'releaf' => link('Releaf', 'https://github.com/cubesystems/releaf'),
      'Releaf' => link('Releaf', 'https://github.com/cubesystems/releaf'),
      'FreeBSD' => link('FreeBSD', 'https://www.freebsd.org/')
    }
  end
end
