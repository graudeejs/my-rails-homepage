class AdminProfile < Releaf::Permissions::User
  validates_length_of :token, minimum: 32, maximum: 255, if: :token?
  validates_uniqueness_of :token, allow_blank: true, allow_nil: true

  has_many :bookmarks, foreign_key: :user_id
end
