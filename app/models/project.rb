class Project < ActiveRecord::Base
  include Publishable

  acts_as_list column: :item_position

  translates :title, :url, :description_html
  globalize_accessors

  dragonfly_accessor :cover

  globalize_locales.map do |locale|
    validates_presence_of("title_#{locale}")
  end

  default_scope ->() { order(:item_position) }

  def releaf_title
    title
  end

  def cover_thumb_url(size)
    return nil if cover.blank?
    cover.thumb(size).encode(:jpg, '-quality 85').url
  end

  def linked_description_html
    LinkedText.new(description_html).call
  end
end
