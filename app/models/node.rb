class Node < ActiveRecord::Base
  include Releaf::Content::Node

  validates_with Releaf::Content::Node::RootValidator, allow: [HomeController]
  validates_with Releaf::Content::Node::SinglenessValidator, under: [HomeController], for: [
    ProjectsController
  ]

  scope :visible, -> { active.where(visible: true) }

  def locale_selection_enabled?
    root?
  end
end
