module Publishable
  def self.included(base)
    base.before_validation :maintain_published_at
    base.scope :published, ->() { base.where.not(published_at: nil) }
  end

  def published=(val)
    @published  = ['0', false, nil].exclude?(val)
  end

  def published
    @published || published_at?
  end

  private

  def maintain_published_at
    self.published_at = published ? Time.current : nil
  end

end
