class TextPage < ActiveRecord::Base
  acts_as_node
  validates_presence_of :text_html

  def linked_text_html
    TextLinker.new(text_html).call
  end
end
