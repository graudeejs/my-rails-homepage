class Bookmark < ActiveRecord::Base
  validates_presence_of :url
  validates_presence_of :title

  belongs_to :user, class_name: 'Releaf::Permissions::User'

  def releaf_title
    title
  end

  def tags=(new_tags)
    if new_tags.blank?
      super []
    elsif new_tags.is_a? String
      super new_tags.split(' ')
    else
      super new_tags
    end
  end
end
