class SocialLink < ActiveRecord::Base
  validates_presence_of :title, :url, :icon_name
  validates :url, url: { scheme: %w[http https] }, unless: :email_link?

  alias_attribute :releaf_title, :title

  private

  def email_link?
    url.to_s.start_with?('mailto:')
  end
end
