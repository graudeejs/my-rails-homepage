module Admin
  module Profile
    class FormBuilder < ::Releaf::Permissions::Profile::FormBuilder
      def field_names
        super << :token
      end
    end
  end
end
