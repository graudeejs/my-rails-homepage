module Admin::Nodes
  class FormBuilder < ::Releaf::Content::Nodes::FormBuilder
    def node_fields
      super << :visible
    end
  end
end
