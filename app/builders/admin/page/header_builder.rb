module Admin
  module Page
    class HeaderBuilder < ::Releaf::Builders::Page::HeaderBuilder
      def profile_controller
        '/admin/profile'
      end
    end
  end
end
