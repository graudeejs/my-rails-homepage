module Admin
  module Page
    class LayoutBuilder < ::Releaf::Builders::Page::LayoutBuilder
      def header_builder
        Admin::Page::HeaderBuilder
      end
    end
  end
end
