module Admin
  module Projects
    class FormBuilder < Admin::Homepage::FormBuilder
      def field_names
        %w[title url since_date till_date cover_uid item_position published description_html]
      end

      def render_published
        releaf_boolean_field(:published)
      end

      def render_item_position
        releaf_item_field(:item_position, options: { select_options: item_position_options, include_blank: false })
      end

      private

      def item_position_options
        options_for_select(item_position_select_options, object.item_position)
      end

      def item_position_select_options
        list = [[t('First'), Project.minimum(:item_position)]]

        position_options.each do |project|
          list << [after_text(project), project.item_position.to_i + 1]
        end

        list
      end

      def position_options
        Project.where.not(id: object.id.to_i)
      end

      def after_text(project)
        template = "After %{item}"
        t(template, default: template, item: project.title)
      end

    end
  end
end
