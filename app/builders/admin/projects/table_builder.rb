module Admin
  module Projects
    class TableBuilder < Admin::Homepage::TableBuilder
      def column_names
        %w[title since_date till_date published_at]
      end
    end
  end
end
