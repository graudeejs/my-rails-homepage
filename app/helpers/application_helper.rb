module ApplicationHelper

  def normalized_controller_name
    params[:controller].tr('/', '-')
  end

  def normalized_view_name
    params[:action].dasherize
  end

  def site_content_scope
    [
      'site',
      normalized_controller_name,
      normalized_view_name,
      @node.try!(:id)
    ].select(&:present?).join('.')
  end

  def page_title
      I18n.t('title', scope: site_content_scope)
  end
end
