class HomeController < ApplicationController
  acts_as_node

  def root
    redirect_to_https(default_path)
  end

  def show
    node = root_nodes.find(params[:node_id])
    redirect_to_https(first_child(node).path)
  end

  private

  def redirect_to_https(path)
    current_uri = URI(request.original_url)

    new_uri = URI(path)
    new_uri.port = current_uri.port
    new_uri.host = current_uri.host

    if original_force_ssl?
      new_uri.scheme = 'https'
      new_uri.port = 443
    end

    redirect_to new_uri.to_s
  end

  def first_child(node)
    node.children.visible.first
  end

  def default_path
    first_child(localized_root_node).path
  end

  def localized_root_node
    root_nodes
      .reorder(nodes_order_condition)
      .find_by(locale: client_locale_options)
  end

  def nodes_order_condition
    template = "POSITION(locale IN :ordered_locales)"
    placeholders = {
      ordered_locales: client_locale_options.join(',')
    }
    ActiveRecord::Base.send(:sanitize_sql_array, [template, placeholders])
  end

  def client_locale_options
    [
      session[:public_locale].to_s,
      request_country_code,
      root_nodes.pluck(:locale)
    ].flatten.select(&:present?).uniq
  end

  def request_country_code
    request.env.fetch('HTTP_X_GEOIP_COUNTRY_CODE', 'EN').downcase
  end

  def root_nodes
    Node.visible.roots
  end

  alias_method :original_force_ssl?, :force_ssl?
  def force_ssl?
    # allow this controller to be accessed via http.
    # It will make redirects to propper page,
    # saving 1 unnessacery redirect (http -> https -> target page)
    false
  end
end
