class TextPagesController < ApplicationController
  def show
    @text_page = find_node.content
  end

  private

  def find_node
    Node.active.find(params[:node_id])
  end
end
