class ProjectsController < ApplicationController
  acts_as_node

  def index
    @projects = projects
  end

  private

  def projects
    Project.published
  end
end
