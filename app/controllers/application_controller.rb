class ApplicationController < ActionController::Base
  include Turbolinks::Controller
  force_ssl if: :force_ssl?

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :set_locale
  before_filter :load_root_node
  before_filter :load_requested_node

  if Rails.env.production?
    rescue_from(Exception, with: :internal_error)
  end
  rescue_from(ActiveRecord::RecordNotFound, with: :page_not_found)

  def page_not_found
    respond_to do |format|
      format.any do
        render 'page_not_found.html', status: 404
      end
    end
  end

  private

  def internal_error(exception)
    log_exception(exception)
    report_exception(exception)
  ensure
    respond_to do |format|
      format.any do
        render 'internal_error.html', status: 500
      end
    end
  end

  def report_exception(exception)
    ExceptionNotifier.notify_exception(exception, env: request.env)
  end

  def log_exception(exception)
    log = ["ERROR: #{exception.inspect}"]
    log += exception.backtrace.map do |line|
      '       ' + line
    end

    Rails.logger.debug log.join("\n")
  end

  def set_locale
    return if params[:locale].blank?
    session[:public_locale] = I18n.locale = params[:locale]
  end

  def load_root_node
    @root_node = Node.active.find_by!(locale: I18n.locale)
  end

  def load_requested_node
    return unless params[:node_id]
    @node = Node.active.find(params[:node_id])
  end

  def force_ssl?
    Rails.env.production?
  end
end
