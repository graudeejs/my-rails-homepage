class Admin::ProfileController < Releaf::Permissions::ProfileController
  def self.resource_class
    AdminProfile
  end

  def permitted_params
    super << :token
  end
end
