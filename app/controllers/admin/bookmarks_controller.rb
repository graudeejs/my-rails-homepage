class Admin::BookmarksController < Admin::HomepageController

  private

  def permitted_params
    [
      :title,
      :url,
      :description,
      :public,
      :tags,
      :user_id
    ]
  end

end
