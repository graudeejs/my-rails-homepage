class Admin::NodesController < Releaf::Content::NodesController

  private

  def permitted_params
    super << :visible
  end
end
