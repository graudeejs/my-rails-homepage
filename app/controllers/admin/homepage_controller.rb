class Admin::HomepageController < Releaf::ActionController

  private

  def localized_attribute_names(*names)
    names.flatten.map do |base_name|
      resource_class.globalize_locales.map do |locale|
        "#{base_name}_#{locale}"
      end
    end.flatten
  end
end
