class Admin::ProjectsController < Admin::HomepageController

  private

  def permitted_params
    [
      :since_date,
      :till_date,
      :cover,
      :remove_cover,
      :retained_cover,
      :published,
      :item_position
    ] + localized_attribute_names(:title, :url, :description_html)
  end

end
