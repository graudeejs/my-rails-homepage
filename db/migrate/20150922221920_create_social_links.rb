class CreateSocialLinks < ActiveRecord::Migration
  def change
    create_table :social_links do |t|
      t.string :title
      t.string :url
      t.string :icon_name

      t.timestamps null: false
    end
  end
end
