class CreateBookmarks < ActiveRecord::Migration
  def change
    enable_extension 'uuid-ossp'
    create_table :bookmarks, id: :uuid do |t|
      t.string :title
      t.text :url
      t.text :description
      t.boolean :public, null: false, default: false
      t.string :tags, array: true, defaul: [], index: true

      t.timestamps null: false
    end
  end
end
