class ChangeBookmarkTagsIndexToGin < ActiveRecord::Migration
  def up
    remove_index :bookmarks, :tags
    add_index :bookmarks, :tags, using: 'gin'
  end

  def down
  end
end
