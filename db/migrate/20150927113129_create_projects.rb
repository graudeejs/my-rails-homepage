class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.date :since_date
      t.date :till_date
      t.string :cover_uid
      t.datetime :published_at

      t.timestamps null: false
    end

    create_table :project_translations do |t|
      t.integer :project_id, index: true
      t.string :locale, limit: 2, index: true
      t.string :title
      t.string :url
      t.text :description_html

      t.timestamps null: false
    end
  end
end
