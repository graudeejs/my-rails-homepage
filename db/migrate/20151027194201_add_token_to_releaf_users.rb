class AddTokenToReleafUsers < ActiveRecord::Migration
  def change
    add_column :releaf_users, :token, :string, index: true
  end
end
