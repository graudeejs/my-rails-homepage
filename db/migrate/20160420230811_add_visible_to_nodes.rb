class AddVisibleToNodes < ActiveRecord::Migration
  def up
    add_column :nodes, :visible, :boolean, default: false, null: false
    ActiveRecord::Base.connection.execute("UPDATE nodes SET visible = TRUE")
  end

  def down
    remove_column :nodes, :visible
  end
end
