class AddItemPositionToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :item_position, :integer, index: true
  end
end
