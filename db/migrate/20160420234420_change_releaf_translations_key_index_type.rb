class ChangeReleafTranslationsKeyIndexType < ActiveRecord::Migration
  def up
    remove_index :releaf_translations, :key
    add_index :releaf_translations, :key
  end

  def down
    remove_index :releaf_translations, :key
    add_index :releaf_translations, :key, unique: true
  end
end
