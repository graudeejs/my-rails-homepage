class RenameReleafI18nBackendTables < ActiveRecord::Migration
  def up
    remove_index :releaf_translation_data, name: "index_releaf_translation_data_on_lang_and_translation_id"
    rename_table :releaf_translations, :releaf_i18n_entries
    rename_table :releaf_translation_data, :releaf_i18n_entry_translations
    rename_column :releaf_i18n_entry_translations, :translation_id, :i18n_entry_id
    rename_column :releaf_i18n_entry_translations, :lang, :locale
    rename_column :releaf_i18n_entry_translations, :localization, :text
  end

  def down
    rename_table :releaf_i18n_entries, :releaf_translations
    rename_table :releaf_i18n_entry_translations, :releaf_translation_data
    rename_column :releaf_translation_data, :i18n_entry_id, :translation_id
    rename_column :releaf_translation_data, :locale, :lang
    rename_column :releaf_translation_data, :text, :localization
    add_index :releaf_i18n_entry_translations, [:locale, :i18n_entry_id], unique: true,
      name: :index_releaf_i18n_entry_translations_on_locale_i18n_entry_id
  end
end
