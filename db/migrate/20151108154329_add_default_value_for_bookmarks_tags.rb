class AddDefaultValueForBookmarksTags < ActiveRecord::Migration
  def change
    change_column :bookmarks, :tags, :string, array: true, default: '{}', null: false
  end
end
