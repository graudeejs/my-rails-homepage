class RenameContactPagesToTextPages < ActiveRecord::Migration
  def change
    rename_table :contact_pages, :text_pages
  end
end
