require 'rails_helper'

RSpec.describe SocialLink, type: :model do
  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to validate_presence_of(:url) }
  it { is_expected.to validate_presence_of(:icon_name) }

  describe "#releaf_title" do
    it "returns #title" do
      expect { subject.title = 'foo' }.to change { subject.releaf_title }.from(nil).to('foo')
    end
  end
end
