require 'rails_helper'

RSpec.describe ApplicationController do

  describe "handling ActiveRecord::RecordNotFound" do
    controller do
      def index
        raise ActiveRecord::RecordNotFound
      end
    end

    before do
      get :index
    end

    it "responds with 404" do
      expect( response.status ).to eq 404
    end

    it "renders error page"
  end
end
